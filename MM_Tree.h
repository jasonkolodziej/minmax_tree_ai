// File : MM_Tree.h
// @Orchestrated by : Jason A. Kolodziej
// @Date & Time : 2019-02-08 14:47
// @Copyright (c) 2019 Jason A. Kolodziej. All rights reserved.

#ifndef HW2_MM_TREE_H
#define HW2_MM_TREE_H
////////////////////////////////
///////HELPER DEFINITONS////////
////////////////////////////////
#define AND &&
#define IS ==
#define IS_NOT !=
#define NOT !
#define OR ||
#define BIT_OR |
#define XOR ^
#define SHIFT_LEFT <<
#define SHIFT_RIGHT >>
////////////////////////////////
#include <climits>
#include <vector>
#include <algorithm>
#include <map>
#include <string>
#include <iostream>


using namespace std;

// guaranteeing that BOOLEAN values take up one byte
typedef char _bool;

// colors for terminal error output
const string red_Bcolor("\033[0;41m");
const string white_color("\033[0;97m");
const string yellow_color("\033[0;93m");
const string reset_color("\033[0m");



//min max tree class built as a template
template <typename T>class MM_Tree{

    // children of the current node
    vector<MM_Tree<T>* > _children;
    // pointer pointing back to the parent
    MM_Tree<T>* _pt_bk;
    // is this a max level
    _bool _max = 0;
    // final value the node will hold
    T _f_val = NULL;
    T _alpha = INT_MIN;
    T _beta = INT_MAX;
    void set_prunability(){
        if(_pt_bk IS_NOT nullptr){
            this->_alpha = this->get_parent()->_alpha;
            this->_beta = this->get_parent()->_beta;
        }};

public:
    explicit MM_Tree(int b):_max((char)b){};
    MM_Tree()= default;
    MM_Tree(MM_Tree<T>* prt, T val = NULL):_pt_bk(prt),_f_val(val){
        _max = NOT prt->is_max();
    };
    void insert_child(MM_Tree<T>& c){_children.push_back(&c);};
    const _bool& is_max(){ return _max; };
    void contruct_tree_string(string& text2);
    T& get_f_val() { return _f_val; };
    void set_max(int m = 1){_max = (char)m;};
    T& calculate_f_val(_bool show_prunning = 1);
    T& ccalculate_f_val();
    MM_Tree<T>* get_parent() const {return _pt_bk;}
    ~MM_Tree(){for(decltype(auto) c: _children){delete c;}}
};

template <typename T>
void MM_Tree<T>::contruct_tree_string(string& text2){
    // saw_p - saw a open parenthesis
    // on_num - number > 9 ; < -9
    // cur_mod - currently modifying tree ( used to help with tree embedded at the front of another tree )
    _bool saw_p, on_num, cur_mod;
    saw_p = on_num = 0;
    cur_mod = 1;
    MM_Tree<T>* go_back = this;
    MM_Tree<T>* current;
    text2.erase(0, 1);
    text2.erase(text2.end() - 1, text2.end());
    for(decltype(auto) s : text2){
        if(s IS ',') on_num = 0;
        if(s IS '('){
            if(NOT cur_mod AND saw_p){
                current = new MM_Tree<T>(current);
            }else{
                current = new MM_Tree<T>(go_back);
            }
            saw_p = 1;
            cur_mod = 0;
        }
        else if (s IS_NOT ')' AND s IS_NOT ','){
            cur_mod = 1;
            if(saw_p){
                go_back = current;
                saw_p = 0;
            }
            if(NOT on_num){
                on_num = 1;
                current = new MM_Tree<T>(go_back, stoi(&s));
                go_back->insert_child(*current);
            }
        }
        else if (s IS ')'){
            MM_Tree<T>* tmp = go_back;
            go_back = go_back->get_parent();
            go_back->insert_child(*tmp);
        }
    }
}

// ccalculate_f_val -> no pruning
// utilizes recursion with the help of c++ lib to help invoke
// a node's children to produce their final value
// so that the current node can decide what its value should be
template <typename T>
T& MM_Tree<T>::ccalculate_f_val(){
    _f_val = (_max) ? (*max_element( _children.begin(), _children.end(),
                                     []( MM_Tree<T>* const a, MM_Tree<T>* const b )
                                     {
                                         if(a->get_f_val() IS NULL AND a->_children.size()>0)
                                             a->calculate_f_val();
                                         if(b->get_f_val() IS NULL AND b->_children.size()>0)
                                             b->calculate_f_val();
                                         return a->get_f_val() < b->get_f_val();
                                     } ))->get_f_val() : (*min_element
            ( _children.begin(), _children.end(),
              []( MM_Tree<T>* const a, MM_Tree<T>* const b )
              {
                  if(a->get_f_val() IS NULL AND a->_children.size()>0)
                      a->calculate_f_val();
                  if(b->get_f_val() IS NULL AND b->_children.size()>0)
                      b->calculate_f_val();
                  return a->get_f_val() < b->get_f_val();
              } ))->get_f_val();
    return _f_val;
}


// calculate_f_val -
// utilizes recursion to help invoke a node's children to produce their final value
// so that the current node can decide what its value should be
template <typename T>
T& MM_Tree<T>::calculate_f_val(_bool show_pruning){
    int r_val = 0;
    for(decltype(auto) c : this->_children){
        // copy the values from the parent
        c->set_prunability();
        // dig to the bottom
        if(c->_children.size()>0) {
            c->calculate_f_val(show_pruning);
        }
        MM_Tree<T>* p = c->get_parent();
        const _bool m = p->is_max();
        int& a = p->_alpha;
        int& b = p->_beta;
        // not maximizing parent
        if(NOT m) {
            _f_val = b = min(b,c->get_f_val());
            if(b <= a){
                if(show_pruning) cout << "alpha pruning\n";
                break;
            }
        }
        // maximizing parent
        else{
            _f_val = a = max(a,c->get_f_val());
            if(b <= a){
                if(show_pruning) cout << "beta pruning\n";
                break;
            }
        }
    }
    return _f_val;
}

#endif //HW2_MM_TREE_H
