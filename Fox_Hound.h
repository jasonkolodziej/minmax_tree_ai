// File : Fox_Hound.h
// @Orchestrated by : Jason A. Kolodziej
// @Date & Time : 2019-02-13 16:28
// @Copyright (c) 2019 Jason A. Kolodziej. All rights reserved.

#ifndef HW2_FOX_HOUND_H
#define HW2_FOX_HOUND_H
#include "MM_Tree.h"
#include <sstream>
#include <iostream>
#define TOM 'o'
#define RAM_SET "xxxx"
#define RAM 'x'
#define E '\0'
/// conversions for user input to computation ///
#define USER_SAYS(x) (--x)
#define AI_SAYS(x) (++x)
// integer to char
#define IC(x) ((char)x)
// char to integer
#define CI(x) ((int)x)
/////////////////////////////////////////////////
//////////////////// SENSORS ////////////////////
// not a valid move, SENSOR
#define NO_MOVE -1
#define END NO_MOVE
// board size limit, SENSOR
#define B_LIMIT ' '
/////////////////////////////////////////////////
/////////////// AI KNOWLEDGE BASE ///////////////
// define function to see if position is in the game board range
#define GBR(p,v,n) ((((CI(p)+(n*v))>=0) AND ((CI(p)+(n*v))<=32)) ? ((CI(p)+(n*v))) : (NO_MOVE))
// check if the user input is valid for the board, if so convert it for the computer ? not, ask for input again
#define VALID_INPUT(u) (((u > B_LIMIT) OR (CI(u)<=0)) ? (NO_MOVE) : ((u)))
/////////////////////////////////////////////////
typedef char pl;
typedef string board;
const string rsq("\033[0;105m\033[0;95m");
const string bsq("\033[0;104m\033[0;97m");



class FoxHoundGame{
    // game board
    board gb;
    // user requesting to move a ram to a specific location...
    pl move, where, f_pos;
    // r[0] - NW ; r[1] - NE ; r[2] - SW ; r[3] - SE  ( acceptable positions on the the game board )
    // used to generate both available moves for the fox or hound
    board _moves;
    // hound positions on the game board ( gb )
    board h_pos;
    // board positions that are not able to have a piece on them
    board bound;
    // swaps two locations on the game board ( default --> 0 ; fox )
    _bool swap(pl who = 0);
    // is the game finished
    _bool finished();
    // main function to handle user and fox, called by main()
    _bool move_pos(_bool& wrong_mv);
    // default - who --> fox == 0
    // default - position --> fox's starting position in on the board
    //              ( this would be 0 in the string, after conversion )
    // find the available positions for a piece on the current board
    ////// apart of the INTELLIGENCE AGENT ///////
    void aval_pos(pl who = 0,pl pos = 1);
    // get the fox to move
    void move_f();
    // min max utility function
    void mm_utility(pl level = 2);
    // future hound moves
    board future_hound();
    // future fox moves
    void future_fox(board& b);

public:
    FoxHoundGame(){
        move = where = 0;
        // optimized s.t. fox does not choose 4, giving the starting optimized choice
        f_pos = IC((rand() % 3 + 1));
        char fill, fill2;
        fill = fill2 = f_pos - IC(1);
        while(fill){gb+=E; --fill;}
        gb+=TOM;
        for(int i=0; i<(27-CI(fill2)); ++i) gb += E;
        // for filling up the board positions that are not allowed to have a player piece on them
        for(int i=0; i<B_LIMIT; ++i) bound += E;
        gb += RAM_SET;
        h_pos+=IC(29);
        h_pos+=IC(30);
        h_pos+=IC(31);
        h_pos+=IC(32);
        // cout << gb << endl << bound << endl;
        print_board();
    }
    // ask the user for their next move, then calls move_pos()
    void ask_mv(){
        _bool wrong_mv = 0;
        while( move_pos(wrong_mv) IS 1 );
    }
    // print the game board with borders
    void print_board();
    //
    board aval_moves(pl who = 0);
};
#endif //HW2_FOX_HOUND_H
