// File : hw2pr3.cpp
// @Orchestrated by : Jason A. Kolodziej
// @Date & Time : 2019-02-11 11:36
// @Copyright (c) 2019 Jason A. Kolodziej. All rights reserved.
///////Example
//this format for the game:
//Rams are on squares 29 to 32 (diagonally forward one square)
//Tom Brady picks square 1 (diagonally forward or backward one square)
//Which Ram do you want to move? 30
//To which square? 26
//Tom Brady moves to square 6
//and so on.
///////////////
// Tom - 'o' -> 6f
// Ram - 'x' -> 78
/// 4 or 5 /// count
////////////
#include "Fox_Hound.h"
void FoxHoundGame::print_board(){
    cout << endl;
    int j = 0;
    for(int i = 31; i>-1; --i){
        if(j IS 4){
            j = 0;
            cout<< reset_color << endl;
        }
        switch(i){
            case 24 ... 27:
            case 16 ... 19:
            case 8 ... 11:
            case 0 ... 3:
                //cout << "i is " << i << " ";
                cout  << bsq << gb[i] << " " << reset_color << rsq << bound[i] << " " << reset_color;
                break;
            default:
                //cout << "i is " << i << " ";
                cout << rsq << bound[i]<< " "  << reset_color << bsq << gb[i] << " "  << reset_color;
                break;

            }
        ++j;
    }
    cout << reset_color;
    cout << endl;
}

_bool FoxHoundGame::swap(pl who){
    USER_SAYS(where);
    if(NOT who){gb[where] = TOM;}
    else if(gb[where] IS_NOT E) return 1;
    else{
        aval_pos(1,move);
        // is piece moving near the user specified where location
        AI_SAYS(where);
        if(_moves.find(where) IS END) return 2;
        USER_SAYS(where);
        gb[where] = RAM;
    }
    USER_SAYS(move);
    gb[move] = E;
    return 0;
}

board FoxHoundGame::aval_moves(pl who){
    (who IS 0) ? aval_pos() :
    aval_pos(1,who);
    return _moves;
}

//char FoxHoundGame::find_real_pos(pl who){
//   return IC(gb.find_first_of(who) + 1);
//}

// calculate current available positions for pl
// who - which piece that is moving ( fox or hound )
// pos - who's current position on the board ( index of the string )
void FoxHoundGame::aval_pos(pl who, pl pos){
    // board based values for user based input
    // maximum ways to move is 4
    string& r = _moves;
    r = "";
    r.resize(4);
    switch (int x = CI(pos))
    {
        // right side oriented row
        case 5 ... 8:
        case 13 ... 16:
        case 21 ... 24:
        case 29 ... 32:
            // check to see if the piece is all the way to the right
            switch(x){
                case 5:
                case 13:
                case 21:
                case 29:   //NW                                          //NE
                    r[0]= (NOT who) ? IC(GBR(pos,5,1)) : IC(NO_MOVE); r[1] = IC(NO_MOVE);
                    //SW                        //SE
                    r[2] = IC(GBR(pos,4,-1)); r[3]=IC(NO_MOVE);
                    break;
                default:
                    // not all the way to the right side
                    r[0]= (NOT who) ? IC(GBR(pos,4,1)) : IC(NO_MOVE); r[1] = (NOT who) ? IC(GBR(pos,3,1)):IC(NO_MOVE);
                    r[2] = IC(GBR(pos,4,-1)); r[3]= IC(GBR(pos,5,-1));
                    break;
            }
            break;
        default:
            // left side oriented row
            switch(x){
                // check and see if the if piece is all the way to the left
                case 1 ... 3:
                    r[0]= (NOT who) ? IC(GBR(pos,5,1)):IC(NO_MOVE); r[1] = (NOT who) ? IC(GBR(pos,4,1)) : IC(NO_MOVE);
                    r[2] = IC(NO_MOVE); r[3]=IC(NO_MOVE);
                    break;
                case 4:
                    r[0] = IC(NO_MOVE); r[1] = (NOT who) ? IC(GBR(pos,4,1)) : IC(NO_MOVE);
                    r[2] = IC(NO_MOVE); r[3] = IC(NO_MOVE);
                    break;
                case 12:
                case 20:
                case 28:
                    r[0]=IC(NO_MOVE); r[1]=(NOT who) ? IC(GBR(pos,4,1)) : IC(NO_MOVE);
                    r[2] = IC(NO_MOVE); r[3]= IC(GBR(pos,4,-1));
                    break;
                default:
                    // not all the way to the left
                    r[0]= (NOT who) ? IC(GBR(pos,5,1)): IC(NO_MOVE); r[1] = (NOT who) ? IC(GBR(pos,4,1)) : IC(NO_MOVE);
                    r[2] = IC(GBR(pos,3,-1)); r[3]= IC(GBR(pos,4,-1));
                    break;
            }
            break;
    }
    if(NOT who){
        for(decltype(auto) h : h_pos){
            // if a hound is in the spot for the fox make it -1
            if(int h_at = _moves.find(h) IS_NOT END){
                _moves[h_at] = IC(-1);
            }
        }
    }
}
_bool FoxHoundGame::move_pos(_bool& wrong_mv){
    if(NOT wrong_mv) move_f();
    wrong_mv = 0;
    if(finished()) return 0;
    int m, w;
    cout << "\nWhich Ram do you want to move? ";
    cin >> m; move = IC(m); pl temp = move; USER_SAYS(temp);
    if(VALID_INPUT(move) IS NO_MOVE) {
        cout << red_Bcolor << white_color << "tile " << CI(move) << " is invalid!" << reset_color << endl;
        wrong_mv = 1;
        return 1;
    }

    if(gb[temp] IS_NOT RAM) {
        cout << red_Bcolor << white_color << "ram not at tile " << CI(AI_SAYS(temp)) << '!' << reset_color << endl;
        wrong_mv = 1;
        return 1;
    }
    cout << flush << "To which square? ";
    cin >> w; where=IC(w);
    if(VALID_INPUT(where) IS NO_MOVE) { //AI_SAYS() is not called because USER_SAYS() was not called in VALID_INPUT()
        cout << red_Bcolor << white_color << "tile " << CI(where) << " cannot be played!" << reset_color<<endl;
        wrong_mv = 1;
        return 1;
    }
    // there was a player at the piece user was wanting to swap
    const _bool s = swap(1);
    if(s IS 1){
        cout << red_Bcolor << white_color << "tile " << CI(AI_SAYS(where)) << " is in use!" << reset_color << endl;
        wrong_mv = 1;
        return 1;
    }
    else if(s IS 2){
        cout << red_Bcolor << white_color << "tile " << CI(AI_SAYS(where)) << " is not a valid move!" << reset_color << endl;
        wrong_mv = 1;
        return 1;
    }
    system("clear");
    print_board();
    return 1;
}

_bool FoxHoundGame::finished() { // might have an issue in the corner with fox and hounds
    const string f_corners = "145";
    string tmps = gb.substr(28,32);
    if(tmps.find(TOM) IS_NOT END) return 1;
    _bool past, by;
    past = by = 0;
    aval_pos(0,f_pos);
    cout << "Looking @ future moves ";
    for(decltype(auto) m : _moves) cout << CI(m) << " ";
    cout << endl;
    // is the fox surrounded ??
    //fox's position current set from user's prospective,
    // create temp and set it to computer perspective
    char temp = f_pos;
    USER_SAYS(temp);
    // worst case have to find all hounds
    // is the fox above all the hounds ??
    int hh = 0;
    for(int h = 0; h<B_LIMIT; ++h) {
        if(gb[h] IS RAM) {
            by = (_moves.find(IC(h)) IS_NOT END) ? ++by : by;
            hh = h;
            h_pos += IC(USER_SAYS(hh));
            // where fox is at based off string positioning
            past = (temp > IC(h)) ? ++past : past;
        }
    }
    // if surr is 4 then the fox is passed all of them, the fox wins
    if((past IS IC(4)) OR ((by IS IC(2)) OR (by IS IC(4)))) return 1;
    if ((f_pos IS IC(4)) AND h_pos.find(IC(8)) IS_NOT END) return 1;
    else if((f_pos IS IC(5) OR f_pos IS IC(1)) AND
    ( (h_pos.find(IC(6)) IS_NOT END) AND ( h_pos.find(IC(9)) IS_NOT END))) return 1;
    return 0;
}

void FoxHoundGame::move_f() {

    // tell swap() who to move
    move = f_pos;
    cout << "Tom currently at " << CI(f_pos) << endl;
    cout << "Moves for TOM ";
    for(decltype(auto) m : _moves) cout << CI(m) << " ";
    cout << endl;
    // calculated move
    mm_utility();
    //f_pos = where = *max_element(_moves.begin(),_moves.end());
    swap();
    print_board();
    cout << "Tom moved to tile " << CI(f_pos) << endl;
    // refresh where to the user value of fox on board
    where = f_pos;
}

void FoxHoundGame::mm_utility(pl level) {
        // current possible moves for fox
        board f_max_root;
        f_max_root+="(";
        future_fox(f_max_root);
        f_max_root += ")";
        MM_Tree<int> max(1);
        max.contruct_tree_string(f_max_root);
        f_pos = where = IC(max.calculate_f_val(0));
}

board FoxHoundGame::future_hound() {
    board b;
    aval_pos(0,f_pos);
    b=+"(";
    const string fm = _moves;
    for(decltype(auto) m : fm) {
       if(m IS_NOT IC(-1)){
           b+= to_string(m);
           b+=",";
       }
    }
    b.pop_back();
    b+=")";
    return b;
}

void FoxHoundGame::future_fox(board &b) {
    for(decltype(auto) h : h_pos){
        aval_pos(1,h);
        const string hm = _moves;
        b+="(";
        for(decltype(auto) m : hm){
            if(m IS_NOT IC(-1)){
                b+= to_string(m);
                b+=',';
            }
        }
        b+=future_hound();
        b+="),";
    }
    b.pop_back();
}

int main() {
 FoxHoundGame g;
 g.ask_mv();
// g.ask_mv();
 //string r = g.aval_moves(32);
 //for(int i = 0; i <4; ++i) cout << " " << CI(r[i]);
 //g.ask_mv();
// printf("%x %x %x %x %x %x %x %x",IC(5),IC(8),IC(13),IC(16),IC(21),IC(24),IC(29),IC(32));
//
// cout << "input: ";
// int j;
// cin >> j;
//    char c = IC(j);
// cout << IC(j) << 'j' << CI('\r') <<" convert";

}