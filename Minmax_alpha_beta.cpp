////////////////////////////////////////////
// File : hw2pr1_2.cpp
// @Orchestrated by : Jason A. Kolodziej
// @Date & Time : 2019-02-05 11:36
// @Copyright (c) 2019 Jason A. Kolodziej. All rights reserved.
//#include "MM_Tree.h"
#include <sstream>
#include <iostream>
#include "MM_Tree.h"
int main(int argc,char* argv[]) {
    if(argc>2){
        cout << red_Bcolor
                << white_color
                <<"WARNING : check user input !"
                << reset_color
                << endl
                << yellow_color
                << "example : " << "((3,12,8),(2,4,6),(14,5,2))"
                << reset_color
                << endl;
        exit(1);
    }
    else{
        string text2(argv[1]);
        MM_Tree<int> tryt(1);
        tryt.contruct_tree_string(text2);
        cout << endl << "Min-Max Tree Optimal Decision : \n" << tryt.calculate_f_val() << endl;
        tryt.set_max(0);
    }
    return 0;
}
